<%-- 
    Document   : chatView
    Created on : 13/07/2015, 14:59:27
    Author     : pamelatabak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chat com WebSocket: Pamela Tabak e Pedro Eusebio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSS -->
        <!--<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/app.min.css">
        <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">
        <link rel="stylesheet" href="css/chatView.css">
        <!--                <link rel="stylesheet" href="css/animate.css">
                        <link rel="stylesheet" href="css/font-awesome.css">
                        <link rel="stylesheet" href="css/jquery.gritter.css"> 
                        <link rel="stylesheet" href="css/toastr.min.css">-->

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- Google fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    </head>
    <body>

        <!-- Header -->
        <header id="header" data-0="top: 0%;" data-400="top: 0%;">
            <div class="container">
                <div class="navbar" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="logo">
                            <a href="index.jsp" title="Home" onclick="exit();"><span></span>ChatWebSocket</a>
                        </h1>
                    </div>

                    <nav class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active sub">
                                <a href="index.jsp" title="Home" onclick="exit();">Home</a>
                            </li>
                            <li class="sub">
                                <a href="about.jsp" title="About" onclick="exit();">About</a>
                            </li>
                            <li class="login"><a href="#" title="Exit" onclick="exit();"><i style="padding-right: 5px;"></i>Exit</a></li>
                        </ul>
                    </nav>
                </div>


                <div class="chat_div">
                    <div id="mainDiv" class="panel panel-info">
                        <div class="panel-heading">
                            CHAT HISTORY &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            <button type="button" id="getOld" class="btn btn-info"  onclick="getOld();"> Load old messages </button>
                            <span id="spanError"></span>
                        </div>
                        <div class="panel-body">
                            <ul class="media-list" id="msgArea">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            ONLINE USERS
                        </div>
                        <div class="panel-body">
                            <ul class="media-list" id="userArea">
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="panel-footer">
                    <div class="input-group">
                        <input type="text" id="msg" class="form-control" placeholder="Enter Message" onkeypress="sendEnter(event);"/>
                        <!--<input type="file" value="Selecione uma imagem" id="image_file" style="width:480px; color: black;"/>-->
                        <span class="input-group-btn">
                            <button class="btn btn-info" type="button" onclick="send();">SEND</button>
                        </span>
                    </div>
                    <span>
                        <input type="file" id="image_file" class="filestyle" data-classButton="btn btn-primary" data-input="false" data-classIcon="icon-plus" data-buttonText="Select an image">
                    </span>

                </div>


                <!--            <div id="menu_id">
                                <ul>
                
                                </ul>
                            </div>
                            <div id="chat_id">
                                <textarea rows="4" cols="100" id="msgArea" readonly style="color: black;"></textarea>
                                <input id="msg" type="text" style="color: black;"/>
                                <button type="submit" id="sendButton" onclick="send();">Send</button>
                            </div>-->
            </div>
        </header>

        <!-- Footer -->
        <footer id="footer">
            <div class="wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3 i">
                            <h2>ChatWebSocket</h2>
                            <div class="content">
                                UFRJ, Centro de Tecnologia<br />
                                Av. Athos da Silveira Ramos<br />
                                Rio de Janeiro, Brasil<br />
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-3 i">
                            <h2>ChatWebSocket</h2>
                            <div class="content clearfix">
                                <a href="index.jsp" title="#" onclick="exit();">Home</a>
                                <a href="about.jsp" title="#" onclick="exit();">About Us</a>
                                <a href="signUp.jsp" title="#" onclick="exit();">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Copyright -->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 i">Copyright © 2015 <a href="#" title="CloudArena">CloudArena</a></div>
                        <div class="col-xs-6 i">Web Design: <a href="#" title="Juraj Molnar" target="_blank">Juraj Molnár</a></div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- JS -->
        <script src="js/chatView.js"></script>
        <script src="assets/js/jquery-1.11.2.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/skrollr.min.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/app.min.js"></script>
        <script src="js/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="js/bootstrap-filestyle.min.js"></script>
    </body>
</html>

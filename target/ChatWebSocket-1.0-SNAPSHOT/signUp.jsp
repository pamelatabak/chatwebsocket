<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chat com WebSocket: Pamela Tabak e Pedro Eusebio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/app.min.css">
        <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- Google fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        
    </head>
    <body class="loginPage">

        <div class="container">
            <a href="index.jsp" title="#" class="back btn-effect">
                <span style="padding-right: 5px;" class="glyphicon glyphicon-arrow-left"></span> Back
            </a>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                    <form class="loginRegistrationForm">
                        <h1 class="text-center">Sign Up</h1>
                        <div>
                            <span id="msg_id" style="color: white; font-size: 20px; text-align: center;"></span>
                        </div>
                        <div class="input">
                            <input type="text" id="user_name_id" name="userName" class="form-control input-field">
                            <label class="input-label">
                                <span class="input-label-content">Name</span>
                            </label>
                        </div>
                        <div class="input">
                            <input type="email" id="user_email_id" name="userEmail" class="form-control input-field">
                            <label class="input-label">
                                <span class="input-label-content">Email Address</span>
                            </label>
                        </div>
                        <div class="input">
                            <input type="text" id="user_login_id" name="userLogin" class="form-control input-field">
                            <label class="input-label">
                                <span class="input-label-content">Login</span>
                            </label>
                        </div>
                        <div class="input">
                            <input type="password" id="user_password_id" name="userPassword" class="form-control input-field">
                            <label class="input-label">
                                <span class="input-label-content">Password</span>
                            </label>
                        </div>
                        <div class="input">
                            <input type="password" id="user_repeat_password_id" name="userRepeatPassword" class="form-control input-field">
                            <label class="input-label">
                                <span class="input-label-content">Repeat Password</span>
                            </label>
                        </div>
                        <input type="button" class="btn btn-lg btn-yellow btn-effect" onclick="makeAjaxRequest();" value="Sign Up"></input>
                    </form>
                </div>
            </div>
        </div>

        <!-- JS -->
        <script src="assets/js/jquery-1.11.2.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/app.min.js"></script>
        <script src="js/signUp.js"></script>

    </body>
</html>

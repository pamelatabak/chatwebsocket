<%-- 
    Document   : index
    Created on : 12/07/2015, 15:27:17
    Author     : pamelatabak
    http://arena.tomaj.sk/v1.4/about.html
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chat com WebSocket: Pamela Tabak e Pedro Eusebio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- CSS -->
        <!--<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/app.min.css">
        <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- Google fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!-- Header -->
        <header id="header" data-0="top: 0%;" data-400="top: 0%;">
            <div class="container">
                <div class="navbar" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="logo">
                            <a href="index.jsp" title="Home"><span></span>ChatWebSocket</a>
                        </h1>
                    </div>

                    <nav class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active sub">
                                <a href="index.jsp" title="Home">Home</a>
                            </li>
                            <li class="sub">
                                <a href="about.jsp" title="About">About</a>
                            </li>
                            <li class="login"><a href="signUp.jsp" title="Sign Up"><i class="glyphicon glyphicon-lock" style="padding-right: 5px;"></i>Sign Up</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="text-center wrap">
                    <h2>Login now on <strong>ChatWebSocket</strong></h2>

                    <!-- Form -->
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <form class="form loginRegistrationForm">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 i">
                                        <div class="input">
                                            <input type="text" class="form-control input-field" id="user_name_id">
                                            <label class="input-label">
                                                <span class="input-label-content">Username</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 i">
                                        <div class="input">
                                            <input type="password" class="form-control input-field" id="user_password_id" onkeypress="sendEnter(event);">
                                            <label class="input-label">
                                                <span class="input-label-content">Password</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 i">
                                        <input type="button" class="btn btn-lg btn-yellow btn-effect" value="Sign in" onclick="makeAjaxRequest();"></input>
                                        <br><br>
                                        <span id="index_msg_id" style="color: white; font-size: 20px; text-align: center;"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </header> 
        
<!--         Features 
	<section class="section features">
		<div class="container">
			<header class="text-center">
				<div class="row">
					<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-8 col-lg-offset-2">
						<h2>Your message for all <strong>customers</strong></h2>
						<p>
                                                  Once you sign in, you will be able to see all messages from all users and broadcast a message to everyone.  
						</p>
					</div>
				</div>
			</header>
			<div class="row text-center">
				<div class="col-sm-4 i">
					<span class="sprite icon icon-responsive"></span>
					<h3>
						<a href="#" title="#">Home</a>
					</h3>
				</div>
				<div class="col-sm-4 i">
					<span class="sprite icon icon-technologies"></span>
					<h3>
						<a href="#" title="#">About Us</a>
					</h3>
				</div>
				<div class="col-sm-4 i">
					<span class="sprite icon icon-animations"></span>
					<h3>
						<a href="#" title="#">Sign Up</a>
					</h3>
				</div>
			</div>
		</div>
	</section>
        -->
        <!-- Footer -->
	<footer id="footer">
		<div class="wrap">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-3 i">
						<h2>ChatWebSocket</h2>
						<div class="content">
							UFRJ, Centro de Tecnologia<br />
							Av. Athos da Silveira Ramos<br />
							Rio de Janeiro, Brasil<br />
						</div>
					</div>
					
					<div class="col-xs-6 col-sm-3 i">
						<h2>ChatWebSocket</h2>
						<div class="content clearfix">
							<a href="index.jsp" title="#">Home</a>
							<a href="about.jsp" title="#">About Us</a>
							<a href="signUp.jsp" title="#">Sign Up</a>
                                                </div>
					</div>
				</div>
			</div>
		</div>

		<!-- Copyright -->
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 i">Copyright © 2015 <a href="http://arena.tomaj.sk/" title="CloudArena">CloudArena</a></div>
					<div class="col-xs-6 i">Web Design: <a href="http://www.jurajmolnar.com/" title="Juraj Molnar" target="_blank">Juraj Molnár</a></div>
				</div>
			</div>
		</div>
	</footer>
        
        <!-- JS -->
        <script src="assets/js/jquery-1.11.2.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/skrollr.min.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/app.min.js"></script>
        <script src="js/index.js"></script>
    </body>
</html>

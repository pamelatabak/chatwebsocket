/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author pamelatabak
 */
@ServerEndpoint("/websocket")
public class WebSocketTest {

    private static Set<Session> clients
            = Collections.synchronizedSet(new HashSet<Session>());
    private static List<String> online_clients = Collections.synchronizedList(new ArrayList<String>());

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        String modified = message;
        try{
        if ("/loginjustentered/".equals(message.substring(0, 18))) {
            String online_user = message.substring(18).split(" ")[0];
            if (!online_clients.contains(online_user)) {
                online_clients.add(online_user);
                modified = message + "/users_online/" + online_clients;
            }
        }
        if("/loginjustexited/".equals(message.substring(0, 17))){
            String online_user = message.substring(17).split(" ")[0];
            if(online_clients.contains(online_user)){
                online_clients.remove(online_user);
            }
        }
        } catch (Exception e){
            
        }

        synchronized (clients) {
            // Iterate over the connected sessions
            // and broadcast the received message
            System.out.println(clients);
            for (Session client : clients) {
                if (!client.equals(session)) {
                    client.getBasicRemote().sendText(modified);
                } else {
                    client.getBasicRemote().sendText(modified);
                }
            }
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        // Add session to the connected sessions set
        clients.add(session);
    }

    @OnClose
    public void onClose(Session session) {
        // Remove session from the connected sessions set
        clients.remove(session);
    }
}

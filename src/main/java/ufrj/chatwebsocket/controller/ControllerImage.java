/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import ufrj.chatwebsocket.model.dao.DaoMessage;
import ufrj.chatwebsocket.model.dao.DaoUser;
import ufrj.chatwebsocket.model.entity.Message;
import ufrj.chatwebsocket.model.entity.User;

/**
 *
 * @author pamelatabak
 */
@WebServlet(name = "ControllerImage", urlPatterns =
{
    "/ControllerImage"
})
@MultipartConfig(location="/tmp", fileSizeThreshold=1024*1024, 
    maxFileSize=1024*1024*5, maxRequestSize=1024*1024*5*5)
public class ControllerImage extends HttpServlet
{
    private int maxFileSize = 2000 * 1024;
    private int maxMemSize = 2000 * 1024;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("application/javascript;charset=UTF-8");
        System.out.println("doPost ControllerImage");
        System.out.println(request);
        Message msg = new Message();
        DaoUser daoUser = new DaoUser();
        DaoMessage daoMessage = new DaoMessage();
        
        msg.setIsImage(0);
        User user = daoUser.getByLogin(request.getParameter("login"));
        System.out.println(request.getParameter("login"));
        msg.setUserId(user.getId());
        int msgId = daoMessage.saveMessage(msg);
        System.out.println("msgId");
        
        msg.setId(msgId);
        msg.setMessage(String.valueOf(msgId) + "." + request.getParameter("extension"));
        msg.setDateTime(request.getParameter("dateTime"));
        
        daoMessage.updateMsg(msg);
        
        boolean submitedFile = false;
        String fileName = String.valueOf(msgId) + "." + request.getParameter("extension");
        Part part = null;
        PrintWriter out = response.getWriter();
        try
        {
            ArrayList parts = (ArrayList) request.getParts();

            Iterator itr = parts.iterator();
            while (itr.hasNext())
            {
                part = (Part) itr.next();
                if (part.getName().compareTo("image") == 0)
                {   
                    String path = this.getServletContext().getRealPath("");
                    String filePath = path + "/img/" + fileName;
                    part.write(filePath);
                    submitedFile = true;
                }
            }
            out.print("img/" + fileName);
            out.flush();
        }
        catch (Exception e)
        {
            out.print(false);
            out.flush();
        }
    }
}

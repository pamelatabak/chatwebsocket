/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import ufrj.chatwebsocket.model.dao.DaoMessage;
import ufrj.chatwebsocket.model.dao.DaoUser;
import ufrj.chatwebsocket.model.entity.Message;
import ufrj.chatwebsocket.model.entity.User;

/**
 *
 * @author pamelatabak
 */
@WebServlet(name = "ControllerMessage", urlPatterns
        = {
            "/ControllerMessage"
        })
public class ControllerMessage extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/javascript;charset=UTF-8");
        PrintWriter out = response.getWriter();
        System.out.println("process request ControllerMessage");

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));

            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject jsonObject = new JSONObject(builder.toString());
            reader.close();
            if (!jsonObject.getString("text").equals("null")) {

                String text = jsonObject.getString("text");
                System.out.println(text);
                String login = jsonObject.getString("login");
                System.out.println(login);
                String dateTime = jsonObject.getString("dateTime");
                System.out.println(dateTime);

                DaoUser daoUser = new DaoUser();
                User user = daoUser.getByLogin(login);

                Message message = new Message();
                message.setIsImage(1);
                message.setMessage(text);
                message.setUserId(user.getId());
                message.setDateTime(dateTime);

                DaoMessage daoMessage = new DaoMessage();
                int id = daoMessage.saveMessage(message);
                System.out.println("id");
            } else if (jsonObject.getString("getOldMessage").equalsIgnoreCase("true")){
                DaoMessage daoMessage = new DaoMessage();
                ArrayList<JSONArray> oldMessages = daoMessage.getOldMessage();
                out.print(oldMessages);
                out.flush();
            } else {
                String login = jsonObject.getString("login");
                String dateTime = jsonObject.getString("dateTime");
                System.out.println(dateTime);
                DaoUser daoUser = new DaoUser();
                User user = daoUser.getByLogin(login);
                DaoMessage daoMessage = new DaoMessage();
                ArrayList<JSONArray> oldMessages = daoMessage.getLastItem(user.getId(), dateTime);
                out.print(oldMessages);
                out.flush();                
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

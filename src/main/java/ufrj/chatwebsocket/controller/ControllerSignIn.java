/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.MessageDigest;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import ufrj.chatwebsocket.model.dao.DaoUser;
import ufrj.chatwebsocket.model.entity.User;

/**
 *
 * @author pamelatabak
 */
@WebServlet(name = "ControllerSignIn", urlPatterns
        = {
            "/ControllerSignIn"
        })
public class ControllerSignIn extends HttpServlet {

    private DaoUser daoUser;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/javascript;charset=UTF-8");
        this.daoUser = new DaoUser();
        PrintWriter out = response.getWriter();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
            StringBuilder builder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject jsonObject = new JSONObject(builder.toString());
            reader.close();

            String login = jsonObject.getString("login");
            String password = jsonObject.getString("password");

            if (daoUser.getByLogin(login) == null) {
                out.print("There is no account with this username.");
            } else {
                User user = daoUser.getByLogin(login);

                String passwordtohash = password;
                String generatedPassword = null;
                // Create MessageDigest instance for MD5
                MessageDigest md = MessageDigest.getInstance("MD5");
                //Add password bytes to digest
                md.update(passwordtohash.getBytes());
                //Get the hash's bytes
                byte[] bytes = md.digest();
                //This bytes[] has bytes in decimal format;
                //Convert it to hexadecimal format
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.length; i++) {
                    sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }
                //Get complete hashed password in hex format
                generatedPassword = sb.toString();
                if (!user.getPassword().equals(generatedPassword)) {
                    out.print("Password and/or username incorrect");
                } else {
                    out.println("ok");
                    
                    System.out.println("before");
                    System.out.println(user.toJSON());
                    out.println(user.toJSON());
                }
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

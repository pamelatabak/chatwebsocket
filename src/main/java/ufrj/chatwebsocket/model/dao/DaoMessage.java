/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import ufrj.chatwebsocket.model.entity.Message;

/**
 *
 * @author pamelatabak
 */
public class DaoMessage extends MainDao
{
    public int saveMessage(Message msg)
    {
        int msgPrimaryKey = 0;
        try
        {
            String insertQuery = "INSERT INTO \"Message\" (message, isimage, userid, datetime) "
                    + "values ('" + msg.getMessage() + "', '" + msg.getIsImage() + "', '" + msg.getUserId() + "', '"
                    + msg.getDateTime() + "');";
            Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement(insertQuery, new String[]
            {
                "id"
            });
            pstmt.executeUpdate();
            
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (null != generatedKeys && generatedKeys.next())
            {
                msgPrimaryKey = (int) generatedKeys.getLong(1);
            }
            con.close();
        } catch (Exception e)
        {
          e.printStackTrace();
        }
        return msgPrimaryKey;
    }
    
    public void updateMsg(Message msg)
    {
        try
        {
            Connection con = getConnection();
            String updateQuery = "UPDATE \"Message\" SET isimage  = '" + msg.getIsImage() + "', userid = '" 
                    + msg.getUserId() + "', message = '" + msg.getMessage() + "', datetime = '" + msg.getDateTime()
                    + "' WHERE id = " + msg.getId();
            PreparedStatement pstmt = con.prepareStatement(updateQuery);
            pstmt.executeUpdate();
            
            con.close();
        } catch (Exception e)
        {
            
        }
    }
    public ArrayList<JSONArray> getLastItem(int id , String dateTime){
        ArrayList<JSONArray> answer = new ArrayList<JSONArray>();
        float max_id = 0;
        try{
            Connection con = getConnection();
            String updateQuery = "SELECT min(id) FROM \"Message\" WHERE userid="+id+" and datetime='"+dateTime+"';";
            System.out.println(updateQuery);
            PreparedStatement pstmt = con.prepareStatement(updateQuery);
            ResultSet rst = pstmt.executeQuery();
            while(rst.next()){
                max_id = Float.parseFloat(rst.getString("min"));
            }
            
            if(max_id-1 >=50){
                updateQuery = "SELECT * FROM \"Message\"  where id>"+(max_id-51)+" AND id< "+(max_id-2)+";";
            } else{
                updateQuery = "SELECT * FROM \"Message\"  WHERE id<"+(max_id-2)+" ;";
            }
            System.out.println(updateQuery);
            pstmt = con.prepareStatement(updateQuery);
            rst = pstmt.executeQuery();
            while(rst.next()){
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(new JSONObject().put("id", rst.getString("id")));
                jsonArray.put(new JSONObject().put("message", rst.getString("message")));
                jsonArray.put(new JSONObject().put("isimage", rst.getString("isimage")));
                DaoUser daouser = new DaoUser();
                jsonArray.put(new JSONObject().put("login", daouser.getById(rst.getString("userid")).getLogin()));
                jsonArray.put(new JSONObject().put("datetime", rst.getString("datetime")));
                answer.add(jsonArray);
            }
            con.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return answer;
    }
    
    public ArrayList<JSONArray> getOldMessage(){
        ArrayList<JSONArray> answer = new ArrayList<JSONArray>();
        float maxId = 0;
        try{
            Connection con = getConnection();
            String selectQuery = "select max(id) from \"Message\";";
            PreparedStatement pstmt = con.prepareStatement(selectQuery);
            ResultSet rst = pstmt.executeQuery();
            while(rst.next()){
                maxId = Float.valueOf(rst.getString("max"));
            }
            if(maxId >= 50){
                selectQuery = "SELECT * FROM \"Message\" where  id>"+(maxId-51)+" and id<"+(maxId-2)+";";
            }else {
                selectQuery = "SELECT * FROM \"Message\" where id<"+(maxId-2)+" ;";
            }
            pstmt = con.prepareStatement(selectQuery);
            rst = pstmt.executeQuery();
            while(rst.next()){
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(new JSONObject().put("id", rst.getString("id")));
                jsonArray.put(new JSONObject().put("message", rst.getString("message")));
                jsonArray.put(new JSONObject().put("isimage", rst.getString("isimage")));
                DaoUser daouser = new DaoUser();
                jsonArray.put(new JSONObject().put("login", daouser.getById(rst.getString("userid")).getLogin()));
                jsonArray.put(new JSONObject().put("datetime", rst.getString("datetime")));
                answer.add(jsonArray);
            }
            con.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        return answer;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.model.dao;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;


/**
 *
 * @author pamelatabak
 */
public class MainDao
{
    private DataSource ds;
    
    public MainDao()
    {
        try
        {
            InitialContext cxt = new InitialContext();
            if (cxt == null)
            {
                System.out.println("[MainDao.constructor] Falha no InitialContext.");
            }
            else
            {
                ds = (DataSource) cxt.lookup("java:comp/env/jdbc/chat");
            }
        } catch (Exception e)
        {
            System.out.println("[MainDao.constructor] Excessão: " + e.getMessage());
        }
    }
    
    public Connection getConnection()
    {
        try
        {
            if (ds != null)
            {
                System.out.println("MainDao: connected");
                return ds.getConnection();
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}

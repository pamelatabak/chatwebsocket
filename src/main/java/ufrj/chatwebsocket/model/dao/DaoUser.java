/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import ufrj.chatwebsocket.model.entity.User;

/**
 *
 * @author pamelatabak
 */
public class DaoUser extends MainDao
{
    public int saveUser(User user)
    {
        System.out.println("save user DaoUser");
        int userPrimaryKey = 0;
        try
        {
            String insertQuery = "INSERT INTO" + "\"User\" " + "(name, login, email, password) "
                    + "VALUES ('" + user.getName() + "', '" + user.getLogin() + "', '" + user.getEmail() 
                    + "', '" + user.getPassword() + "');";
            System.out.println(insertQuery);
            Connection con = getConnection();
            System.out.println("connected!");
//            PreparedStatement pstmt = con.prepareStatement(insertQuery, new String[]
//            {
//                "id"
//            });
            PreparedStatement pstmt = con.prepareStatement(insertQuery);
            pstmt.executeUpdate();
            
//            ResultSet generatedKeys = pstmt.getGeneratedKeys();
//            if (null != generatedKeys && generatedKeys.next())
//            {
//                userPrimaryKey = (int) generatedKeys.getLong(1);
//            }

            con.close();
            
        } catch (Exception e)
        {
            
        }
        return userPrimaryKey;
    }
    
    public User getByLogin(String login)
    {
        User user = null;
        try
        {
            String selectQuery = "SELECT * FROM \"User\" WHERE login = '" + login + "';";
            Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectQuery);
            ResultSet rst = pstmt.executeQuery();
            
            if(rst.next())
            {
                int userId = rst.getInt("id");
                String userName = rst.getString("name");
                String userEmail = rst.getString("email");
                String userLogin = rst.getString("login");
                String userPassword = rst.getString("password");
                user = new User();
                user.setId(userId);
                user.setName(userName);
                user.setEmail(userEmail);
                user.setLogin(userLogin);
                user.setPassword(userPassword);
            } else
            {
               user = null; 
            }
            con.close();
        } catch (Exception e)
        {
            
        }
        return user;
    }
    
    public User getByEmail(String email)
    {
        //we cant have 2 users with the same email
        User user = null;
        try
        {
            String selectQuery = "SELECT * FROM \"User\" WHERE email = '" + email + "';";
            Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectQuery);
            ResultSet rst = pstmt.executeQuery();
            
            if(rst.next())
            {
                int userId = rst.getInt("id");
                String userName = rst.getString("name");
                String userEmail = rst.getString("email");
                String userLogin = rst.getString("login");
                String userPassword = rst.getString("password");
                user = new User();
                user.setId(userId);
                user.setName(userName);
                user.setEmail(userEmail);
                user.setLogin(userLogin);
                user.setPassword(userPassword);
            } else
            {
               user = null; 
            }
            con.close();
        } catch (Exception e)
        {
            
        }
        return user;
    }
   
    public User getById(String id)
    {
        //we cant have 2 users with the same email
        User user = null;
        try
        {
            String selectQuery = "SELECT * FROM \"User\" WHERE id=" + Integer.valueOf(id) + " ;";
            Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectQuery);
            ResultSet rst = pstmt.executeQuery();
            
            if(rst.next())
            {
                int userId = rst.getInt("id");
                String userName = rst.getString("name");
                String userEmail = rst.getString("email");
                String userLogin = rst.getString("login");
                String userPassword = rst.getString("password");
                user = new User();
                user.setId(userId);
                user.setName(userName);
                user.setEmail(userEmail);
                user.setLogin(userLogin);
                user.setPassword(userPassword);
            } else
            {
               user = null; 
            }
            con.close();
        } catch (Exception e)
        {
            
        }
        return user;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.model.entity;

import org.json.JSONObject;

/**
 *
 * @author pamelatabak
 */
public class User
{

    private int id;
    private String name, login, password, email;

    public User()
    {

    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public JSONObject toJSON()
    {
        JSONObject objectJSON = null;
        try
        {
            objectJSON = new JSONObject()
                    .put("id", id)
                    .put("name", name)
                    .put("login", login)
                    .put("email", email);
        }
        catch (Exception e)
        {
        }

        return objectJSON;
    }

    @Override
    public String toString()
    {
        return toJSON().toString();
    }
}

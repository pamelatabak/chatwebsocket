/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufrj.chatwebsocket.model.entity;

/**
 *
 * @author pamelatabak
 */
public class Message
{
    private int id;
    private String message;
    private int isImage;
    //if isImage == 0 : then message is an image. else, it is a text
    private int userId;
    private String dateTime;
    
    public Message()
    {
        
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public int getIsImage()
    {
        return isImage;
    }

    public void setIsImage(int isImage)
    {
        this.isImage = isImage;
    }
    
    public int getUserId()
    {
        return this.userId;
    }
    
    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getDateTime()
    {
        return dateTime;
    }

    public void setDateTime(String dateTime)
    {
        this.dateTime = dateTime;
    }
    
    
}

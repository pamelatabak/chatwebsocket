<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chat com WebSocket: Pamela Tabak e Pedro Eusebio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSS -->
        <!--<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/app.min.css">
        <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.css">

        <!-- Modernizr -->
        <script src="assets/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

        <!-- Google fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <!-- Header -->
        <header id="header" class="subpage" data-0="top: 0%;" data-300="top: -10%;">
            <div class="container">
                <div class="navbar" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="logo">
                            <a href="index.jsp" title="Home"><span></span>ChatWebSocket</a>
                        </h1>
                    </div>

                    <nav class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active sub">
                                <a href="index.jsp" title="Home">Home</a>
                            </li>
                            <li class="sub">
                                <a href="about.jsp" title="About">About</a>
                            </li>
                            <li class="login"><a href="signUp.jsp" title="Sign Up"><i class="glyphicon glyphicon-lock" style="padding-right: 5px;"></i>Sign Up</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="text-center">
                    <h2 class="subpageTitle">About</h2>
                </div>
            </div>
        </header>

        <!-- Blockquote -->
        <div class="section">
            <div class="container">
                <div class="row text-center">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2">
                        <blockquote class="blockquote-lg">ChatWebSocket is a project developed by <strong>Pamela Tabak</strong> and <strong>Pedro Santos Eusebio</strong>, students from Universidade Federal do Rio de Janeiro.
                        The main goal of this project was to learn more about WebSockets, among other stuff.
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer -->
        <footer id="footer">
            <div class="wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3 i">
                            <h2>ChatWebSocket</h2>
                            <div class="content">
                                UFRJ, Centro de Tecnologia<br />
                                Av. Athos da Silveira Ramos<br />
                                Rio de Janeiro, Brasil<br />
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-3 i">
                            <h2>ChatWebSocket</h2>
                            <div class="content clearfix">
                                <a href="index.jsp" title="#">Home</a>
                                <a href="about.jsp" title="#">About Us</a>
                                <a href="signUp.jsp" title="#">Sign Up</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Copyright -->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 i">Copyright © 2015 <a href="http://arena.tomaj.sk/" title="CloudArena">CloudArena</a></div>
                        <div class="col-xs-6 i">Web Design: <a href="http://www.jurajmolnar.com/" title="Juraj Molnar" target="_blank">Juraj Molnár</a></div>
                    </div>
                </div>
            </div>
        </footer>

        <!-- JS -->
        <script src="assets/js/jquery-1.11.2.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/skrollr.min.js"></script>
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/app.min.js"></script>
        <script src="js/index.js"></script>

    </body>
</html>

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function makeAjaxRequest(){
    var isOk = true;
    
    if(document.getElementById('user_name_id').value.trim()===''){
        isOk = false;
        $('#index_msg_id').text("Username can't be empty");
    }
    else if(document.getElementById('user_password_id').value.trim()===''){
        isOk = false;
        $('#index_msg_id').text("Password can't be empty");
    }
    
    if(isOk){
        var sendData = "{ login: " + document.getElementById('user_name_id').value + 
                ", password: " + document.getElementById('user_password_id').value + "}";
        var ajaxRequest = new XMLHttpRequest();
        ajaxRequest.open("POST", "ControllerSignIn");
        ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        ajaxRequest.onreadystatechange =
                    function () {
                        if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                            var response = ajaxRequest.responseText;
                            if(response.substr(0, 2) !=='ok'){
                                $('#index_msg_id').text(response);
                                $('html, body').animate({scrollTop: '0px'}, 800);
                            } else {
                                $('#index_msg_id').text("");
                                localStorage.setItem("user", response.substr(2));
                                window.location.href = "/ChatWebSocket/chatView.jsp";
                            }
                        }
                    };
            ajaxRequest.send(sendData);
    }
}

function sendEnter(event){
    var code = event.keyCode;
    if (code === 13)
        makeAjaxRequest();
}

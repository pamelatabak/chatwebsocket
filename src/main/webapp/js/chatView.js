var login;
var name;
var email;
var id;
var dateTime;
function init() {
    console.log("ready function chatView");
    var user = localStorage.getItem("user");
    if (user !== null) {
        var userParsed = JSON.parse(user);
        console.log(userParsed);
        id = userParsed.id;
        name = userParsed.name;
        login = userParsed.login;
        email = userParsed.email;
        localStorage.clear();
    } else {
        var r = confirm("You have to sign in before trying to enter the chat!");
        if (r === true) {
            window.location.href = '/ChatWebSocket/index.jsp';
        } else {
            window.location.href = '/ChatWebSocket/index.jsp';
        }
        throw new Error("You have to sign in before trying to enter the chat!");
    }
}

var webSocket =
        new WebSocket("ws://" + document.location.host + "/ChatWebSocket/websocket");

webSocket.onerror = function (event) {
    onError(event);
};

webSocket.onopen = function (event) {
    onOpen(event);
};

webSocket.onmessage = function (event) {
    console.log("on message");
    onMessage(event);
    var objDiv = document.getElementById("mainDiv");
    objDiv.scrollTop = objDiv.scrollHeight;
};

function onMessage(event) {
    $('#msgArea').append("<li class='media'>");
    $('#msgArea').append("<div class='media-body'>");
    var currentDate = new Date();
    dateTime = " at " + currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear() + " @ "
            + currentDate.getHours() + ":"
            + currentDate.getMinutes() + ":"
            + currentDate.getSeconds()+ ":"
            + currentDate.getMilliseconds();
    if (event.data.substring(0, 18) === "/loginjustentered/") {
        $('#msgArea').append("<h5>" + event.data.substring(18).split("/users_online/")[0] + "</h5>");
        var user_online = event.data.split("/users_online/")[1].trim();
        user_online = user_online.substring(1, user_online.length - 1).split(",");
        for (var i = 0; i < user_online.length; i++) {
            var username = user_online[i];
            console.log(username);
            var user_class = "." + username;
            if (!$(user_class)[0]) {
                $('#userArea').append("<li class='media'id='" + username + "'>");
                $('#userArea').append("<div class='media " + username + "'>");
                $('#userArea').append("<div class='media-body " + username + "'>");
                $('#userArea').append("<h5 class='" + username + "'>" + username + "</h5>");
                $('#userArea').append("</div>");
                $('#userArea').append("</div>");
                $('#userArea').append("</li>");
            }
        }
    }
    else if (event.data.substring(0, 17) === "/loginjustexited/") {
        username = event.data.substring(17).split(" ")[0];
        username = "." + username;
        console.log(username);
        $(username).remove();
    }
    else if (event.data.substring(0, 9) === "/isImage/") {
        var array = event.data.substring(9).split(" login ");
        var userLogin = array[1].split(" dateTime ")[0];
        var imagePath = array[0];
        dateTime = array[1].split(" dateTime ")[1];
        var imageId = imagePath.replace(".", "");
        var imageId = imageId.replace("/", "");
        $('#msgArea').append("<img id='" + imageId + "' src='" + imagePath + "' onclick=\"swipe('" + imageId + "');\" height='30%' width='30%'><br>");
        $('#msgArea').append("<small class='text-muted'>" + userLogin + dateTime + "</small>");
    }
    else
    {
        console.log(event.data.substring(0, 18));
        var array = event.data.split(" ");
        var userLogin = array[event.data.split(" ").length - 1];
        var msg = event.data.substring(0, event.data.length - userLogin.length);
        console.log(userLogin);
        console.log(msg);
        $('#msgArea').append("<h5>" + msg + "</h5>");
        $('#msgArea').append("<small class='text-muted'>" + userLogin + dateTime + "</small>");
        saveMsg(msg,dateTime);
    }
    $('#msgArea').append("</div>");
    $('#msgArea').append("</li>");
    document.getElementById('msg').value = '';
}

function onOpen(event) {
    init();
    console.log(login);
    if (login !== null)
        webSocket.send("/loginjustentered/" + login + " just entered!");
}

function onError(event) {
    alert(event.data);
}

function send() {
    var txt = document.getElementById('msg').value;
    if (txt.trim() !== '') {
        webSocket.send(txt + " " + login);
        
    }
    if (document.getElementById('image_file').value !== '') {
        //trying to send a picture
        var image = document.getElementById('image_file').value.toLowerCase();
        var arrayImage = image.split(".");
        var extension = arrayImage[arrayImage.length - 1];
        console.log(image);
        console.log(extension);
        if (extension === 'jpeg' || extension === 'png' || extension === 'jpg') {
//            picture will be sent!
            uploadImage(extension);
        } else {
            var r = confirm("You need to choose a .jpeg, .png or .jpg file!");
            if (r === true) {
                document.getElementById('image_file').value = "";
                $(":file").filestyle('clear');
            } else {
                document.getElementById('image_file').value = "";
                $(":file").filestyle('clear');
            }
        }
    }
    return false;
}

function sendEnter(event) {
    var code = event.keyCode;
    if (code === 13)
        send();
}
function exit() {
    webSocket.send("/loginjustexited/" + login + " just exited!");
    webSocket.close();
    window.location.href = '/ChatWebSocket/index.jsp';
}

function uploadImage(extension)
{
    console.log("upload image");

   var currentDate = new Date();
   dateTime = " at " + currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear() + " @ "
            + currentDate.getHours() + ":"
            + currentDate.getMinutes() + ":"
            + currentDate.getSeconds()+ ":"
            + currentDate.getMilliseconds();

    var image = document.getElementById('image_file').files[0];
    var formData = new FormData();
    formData.append("image", image);
    formData.append("login", login);
    formData.append("dateTime", dateTime);
    console.log(formData);
    formData.append("extension", extension);

    var ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("POST", "ControllerImage");
    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
            var response = ajaxRequest.responseText;
            webSocket.send("/isImage/" + response + " login " + login +" dateTime "+dateTime);
            $(":file").filestyle('clear');
        }
    };
    ajaxRequest.send(formData);
}

function swipe(img) {
    console.log(img);
    var largeImage = document.getElementById(img);
    largeImage.style.display = 'block';
    var url = largeImage.getAttribute('src');
    window.open(url, 'Image', 'width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
}

function saveMsg(txt,dateTime) {
    console.log("saveMsg");
//    var currentDate = new Date();
//    var dateTime = " at " + currentDate.getDate() + "/" + (currentDate.getMonth() + 1) + "/" + currentDate.getFullYear() + " @ "
//            + currentDate.getHours() + ":"
//            + currentDate.getMinutes() + ":"
//            + currentDate.getSeconds()+ ":"
//            + currentDate.getMilliseconds();

    var sendData = "{text:'" + txt + "', login:" + login + ", dateTime:'" + dateTime + "', getOldMessage: false }";
    console.log(sendData);
    var ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("POST", "ControllerMessage");
    ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
        }
    };
    ajaxRequest.send(sendData);
}

function getOld() {
    var lastMessage = document.getElementById("msgArea").innerHTML;
    console.log(lastMessage);
    lastMessage = lastMessage.split("<small class=\"text-muted\">");
    console.log(lastMessage);
    console.log(lastMessage.length);
    var sendData;
    if (lastMessage.length !== 1) {
        lastMessage = lastMessage[1];
        console.log(lastMessage);
        lastMessage = lastMessage.split("</small>");
        var userLogin = lastMessage[0].split(" at ")[0];
        var dateTime = lastMessage[0].split(" at ")[1];
        sendData = "{login:" + userLogin + ", dateTime:'" +" at " + dateTime + "', getOldMessage: false, text: null}";
        console.log(sendData);
    } else {

        sendData = "{login:"+null +", dateTime:" + null + ", getOldMessage : true, text: "+ null +"}";
    }
    var ajaxRequest = new XMLHttpRequest();
    ajaxRequest.open("POST", "ControllerMessage");
    ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
            var response = ajaxRequest.responseText;
            console.log(response);
            var arrayParsed = JSON.parse(response);
            if (arrayParsed.length !== 0) {
                for (var i = arrayParsed.length-1; i >= 0; i--) {
                    if (arrayParsed[i][2].isimage == 0) {
                        var input = "<li class='media'>" + "<div class='media-body'>";
                        var id = "img" + arrayParsed[i][1].message.replace(".", "");
                        input += "<img id='" + id + "' src='img/" + arrayParsed[i][1].message + "' onclick=\"swipe('" + id + "');\" height='30%' width='30%'><br>";
                        input += "<small class='text-muted'>" + arrayParsed[i][3].login + arrayParsed[i][4].datetime + "</small>";
                        input += "</div>" + "</li>";
                        $("#msgArea").prepend(input);
                    } else {
                        var input = "<li class='media'>" + "<div class='media-body'>";
                        input += "<h5>" + arrayParsed[i][1].message + "</h5>";
                        input += "<small class='text-muted'>" + arrayParsed[i][3].login + arrayParsed[i][4].datetime + "</small>";
                        input += "</div>" + "</li>";
                        $("#msgArea").prepend(input);
                    }
                }
            } else {
                $("#spanError").text("There are not any messages to be loaded.");
            }

        }
        ;
    }
    ajaxRequest.send(sendData);
}
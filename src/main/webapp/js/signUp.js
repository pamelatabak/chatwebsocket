/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

});

function makeAjaxRequest() {
    var isOk = true;

    if (document.getElementById('user_name_id').value.trim() === '') {
        isOk = false;
        $('html, body').animate({scrollTop: '0px'}, 800);
        $('#msg_id').text("You need to fulfill Name field");
    }
    else if (document.getElementById('user_email_id').value.trim() === '') {
        isOk = false;
        $('html, body').animate({scrollTop: '0px'}, 800);
        $('#msg_id').text("You need to fulfill Email field");
    }
    else if (document.getElementById('user_login_id').value.trim() === '') {
        isOk = false;
        $('html, body').animate({scrollTop: '0px'}, 800);
        $('#msg_id').text("You need to fulfill Login field");
    }
    else if (document.getElementById('user_password_id').value.trim() === '') {
        isOk = false;
        $('html, body').animate({scrollTop: '0px'}, 800);
        $('#msg_id').text("You need to fulfill Password field");
    }
    else if (document.getElementById('user_repeat_password_id').value.trim() === '') {
        isOk = false;
        $('html, body').animate({scrollTop: '0px'}, 800);
        $('#msg_id').text("You need to fulfill Repeat Password field");
    }
    else if (document.getElementById('user_password_id').value !== document.getElementById('user_repeat_password_id').value) {
        isOk = false;
        $('html, body').animate({scrollTop: '0px'}, 800);
        $('#msg_id').text("Password and Repeat Password must contain the same value");
    }

    if (isOk) {
        $('#msg_id').text("");
        var sendData = "{name: " + document.getElementById('user_name_id').value +
                ", email: " + document.getElementById('user_email_id').value +
                ", login: " + document.getElementById('user_login_id').value +
                ", password: " + document.getElementById('user_password_id').value + "}";
        var ajaxRequest = new XMLHttpRequest();
        ajaxRequest.open("POST", "ControllerSignUp");
        ajaxRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        ajaxRequest.onreadystatechange =
                function () {
                    if (ajaxRequest.readyState === 4 && ajaxRequest.status === 200) {
                        var response = ajaxRequest.responseText;
                        //if already exists an account with the login or the email passed
                        //it needs to tell user to select a new
                        if (response !== 'ok') {
                            $('#msg_id').text(response);
                            $('html, body').animate({scrollTop: '0px'}, 800);
                        } else {
                            var r = confirm("Your user has been created. Please sign in now to enter the chat");
                            if (r === true) {
                                window.location.href = '/ChatWebSocket/index.jsp';
                            } else {
                                window.location.href = '/ChatWebSocket/index.jsp';
                            }
                        }
                    }
                };
        ajaxRequest.send(sendData);
    }

}
;